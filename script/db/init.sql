CREATE DATABASE gadget;

\c gadget

CREATE TABLE phone (
    id SERIAL PRIMARY KEY,
    model character varying(255) NOT NULL,
    owner character varying(255) NOT NULL,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

