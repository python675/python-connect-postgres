# Goal for this project
1. Start **Postgresql** in local by **Docker-compose**.
2. Initial database and create table when create Postgresql component on **Docker**.
3. Connect local database to insert/update/delete on phone table.
4. Sample to select insert update delete to **Postgresql**.
***
## Run Postgresql on local
1. Create docker-compose.yml at `./script/docker-compose.yml`
```yaml
version: '3.8'
services:
  db:
    image: postgres:14.1-alpine
    restart: always
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
    ports:
      - '5432:5432'
    volumes:
      - db:/var/lib/postgresql/data
      - ./db/init.sql:/docker-entrypoint-initdb.d/create_tables.sql
volumes:
  db:
    driver: local
```
2. Create initial database script at : `./script/db/init.sql`
```query
CREATE DATABASE gadget;

\c gadget

CREATE TABLE phone (
    id SERIAL PRIMARY KEY,
    model character varying(255) NOT NULL,
    owner character varying(255) NOT NULL,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
```
3. Goto docker-compose.yml folder.
```commandline
cd script
```
4. Start docker
```commandline
docker-compose up
```
***
Reference : 
- Create connection : https://www.postgresqltutorial.com/postgresql-python/connect/
- Python data access : https://www.tutorialspoint.com/python_data_access/python_postgresql_insert_data.htm