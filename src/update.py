from connect import connect
from connect import close
from select import select


def update(model: str, owner: str, index: str):
    conn = connect()
    cursor = conn.cursor()
    command = ''
    if model != '' and owner != '':
        command = f'''UPDATE PHONE SET MODEL = '{model}', OWNER = '{owner}', UPDATED_AT = CURRENT_TIMESTAMP WHERE ID = '{index}';'''
    elif model != '':
        command = f'''UPDATE PHONE SET MODEL = '{model}', UPDATED_AT = CURRENT_TIMESTAMP WHERE ID = '{index}';'''
    elif owner != '':
        command = f'''UPDATE PHONE SET OWNER = '{owner}', UPDATED_AT = CURRENT_TIMESTAMP WHERE ID = '{index}';'''
    cursor.execute(command)
    conn.commit()
    print("Records updated")
    close(conn)


if __name__ == '__main__':
    while True:
        print('All phone')
        select()
        idx = input("Do you want to update index number?")
        mod = input("Do you want update model to?")
        own = input("Do you want update owner to?")
        update(mod, own, idx)
