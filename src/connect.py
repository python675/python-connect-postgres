import psycopg2
from config import config


def connect():
    """ Connect to the PostgreSQL database server """
    conn1 = None
    try:
        params = config()
        print('Connecting to the PostgreSQL database...')
        conn1 = psycopg2.connect(**params)
        conn1.autocommit = True
        cursor = conn1.cursor()
        print('PostgreSQL data version:')
        cursor.execute('SELECT version()')

        db_version = cursor.fetchone()
        print(db_version)

        cursor.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    return conn1


def close(conn2: None):
    if conn2 is not None:
        conn2.close()
        print('Database connection closed.')


if __name__ == '__main__':
    conn = connect()
    close(conn)
