from connect import connect
from connect import close
from phone import Phone


def insert(phone: Phone):
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f'''INSERT INTO PHONE(MODEL, OWNER) VALUES ('{phone.model}', '{phone.owner}')''')
    conn.commit()
    print("Records inserted")
    close(conn)


if __name__ == '__main__':
    while True:
        model = input("What your phone model?")
        owner = input("Who is owner name?")
        inputPhone = Phone(model, owner)
        insert(inputPhone)
