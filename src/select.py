from connect import connect
from connect import close
from phone import Phone


def select():
    conn = connect()
    cursor = conn.cursor()

    cursor.execute('''SELECT * FROM PHONE''')

    # print('Fetch first 1')
    # result = cursor.fetchone()
    # print(result)

    print('Fetch next all')
    # result = cursor.fetchall()
    result = [Phone(r[1], r[2]) for r in cursor.fetchall()]
    for phone in result:
        print(f'model: {phone.model}, owner: {phone.owner}')

    close(conn)


if __name__ == '__main__':
    select()
